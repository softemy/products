__author__ = 'Loi'
import settings
from django.core import mail
from main.models import Account
def extract_fullname(name):
    eles = [e.capitalize() for e in name.split(' ') if e]
    if not eles: raise Exception('BadName')
    firstname = eles[-1]
    lastname = ''
    if len(firstname) == 1 and len(eles) >= 2:
        firstname = ' '.join(eles[-2:])
        lastname = ' '.join(eles[:-2])
    else:
        firstname = eles[-1]
        lastname = ' '.join(eles[:-1])
    return firstname, lastname

def _send_email(subject, message, from_addr=None, to_addr=[]):
    mail.send_mail(settings.EMAIL_SUBJECT_PREFIX + subject,
        message,
        settings.EMAIL_HOST_USER,
        to_addr)


def send_email(subject, message, from_addr=None, to_addr=[]):
    _send_email(subject, message, from_addr, to_addr)

def get_account(request):
    if not request.user.is_authenticated():
        raise Exception('NotAuthenticated')
    try:
        account = Account.objects.get(username=request.user)
        return account
    except Exception as e:
        print e
        raise Exception('User is not a team')