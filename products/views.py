import StringIO
import base64
from google.appengine.api import files
from google.appengine.ext import blobstore

__author__ = 'Loi'
# -*- coding: utf-8 -*-
import simplejson
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from main.forms import *
from django.core.urlresolvers import reverse
from main.models import *
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.db import transaction
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.core.exceptions import ObjectDoesNotExist
from datetime import date, datetime, timedelta
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from settings import MEDIA_ROOT
import random
import string
import qrcode
from utils import send_email, extract_fullname, get_account
ITEM_PER_PAGE = 25


def user_login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('index'))
    if request.method == 'POST':
        # validate the form
        form = AuthenticationForm(data=request.POST)
        print form.errors
        if form.is_valid():
            # log the user in
            login(request, form.get_user())
            return HttpResponseRedirect(reverse('index'))
    else:
        form = AuthenticationForm(request)

    context = RequestContext(request)
    response = render_to_response('login.html', {'form': form}, context_instance=context)
    return response


def index(request):
    context = RequestContext(request)
    response = render_to_response('index.html', {}, context_instance=context)
    return response


def register(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('index'))
    else:
        message = ''
        # test new brance
        register_form = RegisterForm()
        if request.method == 'POST':
            data = request.POST.copy()
            activation_code = ''.join(random.choice(string.ascii_uppercase
                                                    + string.digits) for n in range(25))
            register_form = RegisterForm(data=data)
            if register_form.is_valid():
                try:
                    new_register = register_form.save(commit=False)
                    new_register.activation_code = activation_code
                    new_register.save()
                except Exception as e:
                    print e
                site = request.get_host()
                if site != 'localhost:8000':
                    site = 'http://' + site
                mail_message = u'Please access to %s/activate/%s/ to complete your registration.' \
                               % (site, activation_code)
                send_email(u'Registration at ' + unicode(site), mail_message, \
                    to_addr=[data['email']])

                success = True
                if request.is_ajax():
                    response = simplejson.dumps({
                        'success': success,
                        'message': message,
                        'redirect': reverse('index')
                    })
                    return HttpResponse(response, mimetype='json')
            else:
                message = u"Error at input\n"
                focus_id = ""
                try:
                    old_register = Register.objects.get(email=data['email'])
                    if old_register:
                        message += u': Registered email.'
                        focus_id = u"#id_email"
                except ObjectDoesNotExist:
                    try:
                        old_register = Register.objects.get(username=data['username'])
                        if old_register:
                            message += u': Username is not available.'
                            focus_id = u"#id_username"
                    except ObjectDoesNotExist:
                        pass
                    pass
                success = False
                if request.is_ajax():
                    response = simplejson.dumps({
                        'success': success,
                        'message': message,
                        'focus_id': focus_id,
                        })
                    return HttpResponse(response, mimetype='json')

        context = RequestContext(request)
        return render_to_response('register.html',
            {'message': message,
             'form': register_form},
            context_instance=context)


@transaction.commit_on_success
def activate(request, key):
    template_name = "activate.html"
    try:
        register = Register.objects.get(activation_code=key,
            status__exact='Inactive')
    except Exception as e:
        print e
        message = u'Invalid registration code or this account has been activated.'
        success = False
        return render_to_response(template_name,
            {'message':message, 'success':success})

    register.status='Active'
    register.save()
    message = u'Fullname: %s\nRegistration date: %s\nEmail: %s\n' % (register.fullname,\
                                                                     register.register_date,\
                                                                     register.email,)
    send_email(u'New registration', message,
        to_addr=['loi.luuthe@gmail.com'])
    first_name, last_name = extract_fullname(
        register.fullname)
    raw_password = ''.join(random.choice(string.ascii_letters
                                         + string.digits) for n in range(9))
    user = User.objects.create(
        username=register.username,
        password=make_password(raw_password),
        first_name=first_name,
        last_name=last_name)

    new_account = Account.objects.create(
        username=user,
        fullname=register.fullname,
        email=register.email)

    subject = u'Information of account'
    message = u'Information of account: \nUsername: %s\nPassword: %s\n\nHave nice time.' \
              % (unicode(new_account.username.username), unicode(raw_password))
    send_email(subject, message, to_addr=[new_account.email])

    message = u'Please check your email to get your registration information.'
    success = True
    return render_to_response(template_name,
        {'message': message, 'success' : success})

@transaction.commit_on_success
def create_product(request):
    try:
        user = request.user
    except Exception as e:
        print e
        return HttpResponseRedirect(reverse('login'))
    if not user.is_superuser:
        return HttpResponseRedirect(reverse('login'))
    product_form = ProductForm()
    if request.method == 'POST':
        data = request.POST.copy()
        product_form = ProductForm(data=data)
        if product_form.is_valid():
            try:
                new_product = product_form.save(commit=False)
                new_product.user_id = user
                new_product.save()
            except Exception as e:
                print e
                message = u"Error in input data."
                success = False
                if request.is_ajax():
                    response = simplejson.dumps({
                        'success': success,
                        'message': message
                    })
                    return HttpResponse(response, mimetype='json')

            success = True
            message = u"Successfully created new product."
            if request.is_ajax():
                response = simplejson.dumps({
                    'success': success,
                    'message': message,
                    'redirect': reverse('index')
                })
                return HttpResponse(response, mimetype='json')
        else:
            print product_form._errors
            message = u"Error in input data."
            success = False
            if request.is_ajax():
                response = simplejson.dumps({
                    'success': success,
                    'message': message
                })
                return HttpResponse(response, mimetype='json')

    context = RequestContext(request)
    return render_to_response('create_product.html',
        {'user': user,
         'form': product_form},
        context_instance=context)


def view_product(request, product_id):
    try:
        account = get_account(request)
    except Exception as e:
        account = None

    try:
        product = Product.objects.get(id=product_id, status__exact=1)
    except Exception as e:
        product = None

    context = RequestContext(request)
    return render_to_response('view_product.html',
        {'account': account,
         'product': product},
        context_instance=context)

def view_qrcode(request, product_id):
    product = Product.objects.get(id=product_id, status__exact=1)
    url = request.get_host()
    url = url + "/view_product/" + str(product.id)
    img = qrcode.make(url)
    out = StringIO.StringIO()
    img.save(out,"PNG")
    content = out.getvalue()
    out.close()
    file_name = product.name + '.png'
    response = HttpResponse(content,mimetype="image/png")
    response['Content-Disposition'] = 'attachment; filename="%s"' % (file_name)
    return response

def view_categories(request):
    objects = Category.objects.all()
    category_form = CategoryForm()
    paginator = Paginator(objects, ITEM_PER_PAGE) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        categories = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        categories = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        categories = paginator.page(paginator.num_pages)

    context = RequestContext(request)
    return render_to_response('view_categories.html',
        {'categories': categories,
         'category_form': category_form},
        context_instance=context)


def view_materials(request):
    objects = Material.objects.all()
    material_form = MaterialForm()
    paginator = Paginator(objects, ITEM_PER_PAGE) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        materials = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        materials = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        materials = paginator.page(paginator.num_pages)

    context = RequestContext(request)
    return render_to_response('view_materials.html',
        {'materials': materials,
         'material_form': material_form},
        context_instance=context)


def view_manufacturers(request):
    objects = Manufacturer.objects.all()
    manufacturer_form = ManufacturerForm()
    paginator = Paginator(objects, ITEM_PER_PAGE) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        manufacturers = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        manufacturers = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        manufacturers = paginator.page(paginator.num_pages)

    context = RequestContext(request)
    return render_to_response('view_manufacturers.html',
        {'manufacturers': manufacturers,
         'manufacturer_form': manufacturer_form},
        context_instance=context)

def view_product_in_manufacturer(request, manufacturer_id):
    manufacturer = Manufacturer.objects.get(id=manufacturer_id)
    objects = Product.objects.filter(manufacturer_id=manufacturer, status=1)
    paginator = Paginator(objects, ITEM_PER_PAGE) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        products = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        products = paginator.page(paginator.num_pages)

    context = RequestContext(request)
    return render_to_response('product_in_manufacturer.html',
        {'products': products,
         'manufacturer': manufacturer},
        context_instance=context)


def view_product_in_category(request, category_id):
    category = Category.objects.get(id=category_id)
    objects = Product.objects.filter(category_id=category, status=1)
    paginator = Paginator(objects, ITEM_PER_PAGE) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        products = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        products = paginator.page(paginator.num_pages)

    context = RequestContext(request)
    return render_to_response('product_in_category.html',
        {'products': products,
         'category': category},
        context_instance=context)


def view_product_in_material(request, material_id):
    material = Material.objects.get(id=material_id)
    objects = Product.objects.filter(material_id=material, status=1)
    paginator = Paginator(objects, ITEM_PER_PAGE) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        products = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        products = paginator.page(paginator.num_pages)

    context = RequestContext(request)
    return render_to_response('product_in_material.html',
        {'products': products,
         'material': material},
        context_instance=context)


def all_products(request):
    objects = Product.objects.filter(status=1)
    paginator = Paginator(objects, ITEM_PER_PAGE) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        products = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        products = paginator.page(paginator.num_pages)

    context = RequestContext(request)
    return render_to_response('products.html',
        {'products': products,
         },
        context_instance=context)

def change_password(request):
    try:
        user = request.user
    except Exception as e:
        print e
        return HttpResponseRedirect(reverse('login'))
    if request.method =='GET':
        form = ChangePasswordForm(user)
        context = RequestContext(request)
        return render_to_response('change_password.html',
            {'form':form,
            },
            context_instance=context)
    elif request.method == 'POST':
        form = ChangePasswordForm(user,request.POST)
        if form.is_valid():
            form.save()
            logout(request)
            response = simplejson.dumps({
                'success': True,
                'message': u'Your password has been changed'})
            return HttpResponse(response, mimetype='json')
        else:
            error = {}
            for k, v in form.errors.items():
                error[form[k].auto_id] = form.error_class.as_text(v)
            response = simplejson.dumps({
                'success': False,
                'err': error,
                'message': u'Error at input'})
            return HttpResponse(response, mimetype='json')


@transaction.commit_on_success
def create_manufacturer(request):
    try:
        user = request.user
    except Exception as e:
        print e
        return HttpResponseRedirect(reverse('login'))
    manufacturer_form = ManufacturerForm()
    if request.method == 'POST':
        data = request.POST.copy()
        manufacturer_form = ManufacturerForm(data=data)
        if manufacturer_form.is_valid():
            manufacturer_form.save()
            success = True
            message = u"Successfully created new manufacturer."
            if request.is_ajax():
                response = simplejson.dumps({
                    'success': success,
                    'message': message,
                    'redirect': reverse('index')
                })
                return HttpResponse(response, mimetype='json')
        else:
            print manufacturer_form._errors
            message = u"Error in input data."
            success = False
            if request.is_ajax():
                response = simplejson.dumps({
                    'success': success,
                    'message': message
                })
                return HttpResponse(response, mimetype='json')

    context = RequestContext(request)
    return render_to_response('create_manufacturer.html',
        {'user': user,
         'form': manufacturer_form},
        context_instance=context)

@transaction.commit_on_success
def create_category(request):
    try:
        user = request.user
    except Exception as e:
        print e
        return HttpResponseRedirect(reverse('login'))
    category_form = CategoryForm()
    if request.method == 'POST':
        data = request.POST.copy()
        category_form = CategoryForm(data=data)
        if category_form.is_valid():
            category_form.save()
            success = True
            message = u"Successfully created new category."
            if request.is_ajax():
                response = simplejson.dumps({
                    'success': success,
                    'message': message,
                    'redirect': reverse('index')
                })
                return HttpResponse(response, mimetype='json')
        else:
            print category_form._errors
            message = u"Error in input data."
            success = False
            if request.is_ajax():
                response = simplejson.dumps({
                    'success': success,
                    'message': message
                })
                return HttpResponse(response, mimetype='json')

    context = RequestContext(request)
    return render_to_response('create_category.html',
        {'user': user,
         'form': category_form},
        context_instance=context)


@transaction.commit_on_success
def create_material(request):
    try:
        user = request.user
    except Exception as e:
        print e
        return HttpResponseRedirect(reverse('login'))
    material_form = MaterialForm()
    if request.method == 'POST':
        data = request.POST.copy()
        material_form = MaterialForm(data=data)
        if material_form.is_valid():
            material_form.save()
            success = True
            message = u"Successfully created new material."
            if request.is_ajax():
                response = simplejson.dumps({
                    'success': success,
                    'message': message,
                    'redirect': reverse('index')
                })
                return HttpResponse(response, mimetype='json')
        else:
            print material_form._errors
            message = u"Error in input data."
            success = False
            if request.is_ajax():
                response = simplejson.dumps({
                    'success': success,
                    'message': message
                })
                return HttpResponse(response, mimetype='json')

    context = RequestContext(request)
    return render_to_response('create_material.html',
        {'user': user,
         'form': material_form},
        context_instance=context)
